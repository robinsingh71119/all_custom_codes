<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portfolio</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="portfolio.css"/>
</head>
<body>
    <aside>
        <nav>
            <img src="me.jpg" alt="smily" >
            <a href="#home"><i class="material-icons md-36">home</i></a>
            <p>Home</p>
            <a href="#about"><i class="material-icons md-36">person</i></a>
            <p>About</p>
            <a href="#folio"><i class="material-icons md-36">image</i></a>
            <p>Folio</p>
            <a href="#contact"><i class="material-icons md-36">mail</i></a>
            <p>Contact</p>
        </nav>
    </aside>
    <nav class="mobile-nav">
        <a href="#home">Home</a>
        <a href="#about">About</a>
        <a href="#folio">Folio</a>
        <a href="#contact">Contact</a>

    </nav>
    <section class="allarange">
    <main>

        <header class="home" id="home"><br><br>
            <h1>I'm Robin Singh </h1>
            <h5>Designer//Developer</h5>
            <img src="me.jpg" alt="myself" style="width:50%; height:50%">
        </header>
    </main>
    <section class="about" id="about">
        <h2>About Me</h2>
        <p>
            <span>Hello, I'm Robin Singh </span> I'm Developer and Designer, Life long learner, and ready for new opportunities.
            A bit about me , I like to listen music, Improve my skills, Gain knowleged about new things.  
        </p>
        <p>
            I believe in Team work and user centered desing and developing the best possible experience to people. I have experience with Html, CSS, BootStrap, JavaScript, Jquery, PHP, AJAX, JSON.

        </p>
        <h3>My Skills</h3>
        <p>HTML 5</p>
        <div class="container">
            <div class="skills html">95%</div>
        </div>
        <p>CSS 3</p>
        <div class="container">
            <div class="skills css">93%</div>
        </div>
        <p>BootStrap 4</p>
        <div class="container">
            <div class="skills bootstrap">90%</div>
        </div>
        <p>JavaScript 3</p>
        <div class="container">
            <div class="skills js">85%</div>
        </div>
        <p>JQuery</p>
        <div class="container">
            <div class="skills jq">80%</div>
        </div>
        <p>PHP 7</p>
        <div class="container">
            <div class="skills php">70%</div>
        </div>
        <p>AJAX</p>
        <div class="container">
            <div class="skills ajax">70%</div>
        </div>
        <p>JSON</p>
        <div class="container">
            <div class="skills json">70%</div>
        </div>


    </section>
    <section class="folio" id="folio">
        <h2>Folio</h2>
        <div class="row-halves">
            <div class="col">
                <img src="business.jpg" alt="">
                <h3>Business</h3>
                <p>
                    Building Responsive and thoughtful sites for small or large business

                </p>
            </div>
            <div class="col" >
                <img src="mobile.jpg" alt="" style="width: 60%;height: 40%;margin-left: 20%;">
                <h3>Mobile</h3>
                <p>
                    Fully Responsive, mobile ready desings and implementation for all.
                </p>
            </div>
        </div>
        <div class="row-halves">
            <div class="col">
                <img src="hdseen3.jpg" alt="">
                <h3>UX Design</h3>
                <p>
                    Thoughtful and intuitive user experiences. All of my design s will be guaranteed to work your clients.
                </p>
            </div>
            <div class="col">
                <img src="hdseen4.jpg" alt="">
                <h3>Personal</h3>
                <p>
                    I also design and build for personal project. No idea is too small and everyone deserves a voice on the web.
                </p>
            </div>
        </div>
    </section>
    <section class="contact" id="contact">

        <h2>Contact</h2>
        <div class="container">
            <form action="">
                <label for="name">Name</label>
                <input type="text" id="name" name="name">
                <label for="email">E-mail</label>
                <input type="text" id="email" name="email">
                <label for="message">Message</label>
                <textarea name="message" style="height: 200px"></textarea>
                <input type="submit" value="message">
            </form>
        </div>
    </section>
</section>

    <footer>
        <section class="top-bar">
            <a href=""><i class="fab fa-facebook-f"></i></a>
            <a href=""><i class="fab fa-twitter"></i></a>
            <a href=""><i class="fab fa-github"></i></a>

        </section>
            <section class="bottom-bar">
                <p>&copy; 2019| All rights reserved</p>

            </section>
    </footer>
</body>
</html>