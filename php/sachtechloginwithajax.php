<?php
$connect= new mysqli('localhost','root','','firstdatabase');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Niconne" rel="stylesheet">
    <style>
    .box{
        width:350px;
        height:auto;
        background: white;
        position: absolute;
        display:block;
        margin:auto;
        margin-top:-33%;
        margin-left:40%;
        margin-right:0px;
        margin-bottom:0px;
        padding:40px;
        box-sizing:border-box;
    }
    .blur{
        color:rgba(128, 128, 128, 0.442);
    }
    .form-control{
        border:2px solid rgba(128, 128, 128, 0.333);
    }
    .error{
        font-size:10px;
        color:red;
    }
    </style>
</head>
<body>
<?php
if(isset($_POST['loginusername'])){
    $username=$_POST['loginusername'];
    $password=$_POST['loginpassword'];
    $password=md5($password);
    $qry="SELECT * FROM sachtechdb WHERE username='$username' AND password='$password'";
    $result=$connect->query($qry);
    if($result->num_rows>0){
        echo "success";
        $_SESSION['login']=true;
        header('Location:sachtechdashboardajax.php');

    }else{
        echo "error in code".$connect->error;
        $_SESSION['login']=false;  
    }
}
?>
    <div class="container-fluid">
    <div class="row bg-dark" style="color:white; padding-left:100%; padding-top:25%;  ">
    </div>
    
    <div class="row bg-info" style="color:white; padding-left:100%; padding-top:27%;  ">
    <h1 style="font-size: 50px; position:absolute; top:20%; left:40%;">SachTech Login</h1></div>
    
    <div class="box">
                    <h2 style="text-align:center; color:black;" ><i  class="fas fa-user h4"></i>SIGN IN</h2><hr>
                    <div id="info"></div>
                    <form action="sachtechdashboardajax.php" method="post" class="blur" id="myform">
                        <label for="">USERNAME</label>
                        <input type="text" class="form-control" name="loginusername" id="loginusername" required placeholder="username"/>
                        <label for="" class="mt-2">PASSWORD</label>
                        <input type="text" class="form-control mb-2" name="loginpassword" id="loginpassword" required placeholder="password"/>
                        <a href="" id="passwordid" data-toggle='modal' data-target='#popupmodal' style="font-weight: bold;">Forgot Password!</a><br/>
                        <button type="submit" id="loginbtn" class="form-control bg-success border-white text-white h2 mt-3"><i class="fas fa-sign-in-alt mr-2"></i>Submit</button>
                        <a href="sachtechregisterwithajax.php" class="mt-2 ml-3" style="font-weight: bold;" >Don't Have Account ? Creat now</a>
                        
                    </form>
            </div>
            
    </div>
    </div>
    <div class="modal" id="popupmodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Reset Password</h2>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <!-- <label for="">Username</label>
                <input type="text" id="usernameedit" placeholder="username" class="form-control"/> -->
                <label for="">New Password</label>
                <input type="text" id="passwordedit" placeholder="New Password" class="form-control"/>
                <!-- <label for="">Confirm Password</label>
                <input type="text" id="confirmpassword" placeholder="Comfirm Password" class="form-control"/> -->
                <button class="btn btn-info mt-2" onclick="updatedata()">Update</button>
                </div>
            </div>
        </div>
        </div>
    <script>
    // $(document).ready(function(){
    //     $('#loginbtn').click(function(){
    //         var username=$('#loginusername').val();
    //         var password=$('#loginpassword').val();
    //         if(username!='' && password !=''){
    //             $.ajax({
    //                 url:'sachtechajaxdb.php',
    //                 type:'post',
    //                 data:{username:username,password:password},
    //                 success:function(data){
    //                     if(daya=='no'){
    //                         alert('Wrong Data');
    //                     }
    //                     else{
    //                         window.location='http://localhost/php/sachtechdashboardajax.php';
    //                     }
    //                 }
    //             });
    //         }
    //         else{
    //             alert('both are required');
    //         }
    //     });
    // });
    // $(document).ready(function(){
    //     $("#myform").validate({
    //         rules:{
    //             username:{
    //                 required:true
    //             },password:{
    //                 required:true,
    //                 password:true
                    
    //             }
    //         },
    //         messages:{
    //             username:{
    //                 required:"Enter  username"
    //             },
    //             password:"Enter password"
    //         },
    //         submitHandler:subform
    //     });
    //     function subform(){
    //     var data=$('#myform').serialize();
    //     $.ajax({
    //         url:'sachtechajaxdb.php',
    //         type:'post',
    //         data:data,
    //         beforeSend:function(){
    //             $("#info").fadeOut();
    //             $("#login").html("Sending...");
    //         },
    //         success:function(resp){
    //             if(resp=="ok"){
    //                 $("#login").html("<img src='tenor.gif' width='15'/>");
    //                 setTimeout('window.location.href="sachtechdashboardajax.php"',4000);
    //             }else{
    //                 $('#info').fadeIn(1000,function(){
    //                     $('#info').html("<div class='alert alert-danger'>"+resp+"</div>");
    //                     $('#login').html('Login');
    //                 })
    //             }
    //         }
    //     });
    //     }
    // });
    </script>
</body>
</html>