<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-5">
    <form action="" method="post">
    <h2 class="text-center " style="text-decoration:underline; ">Add Record</h2>
    <label for="" class="mt-2">Username</label>
    <input type="text" class="form-control"  id="username" required placeholder="username"/> 
    <label for="" class="mt-2">Password</label>
    <input type="text" class="form-control" id="password" required placeholder="password"/> 
    <label for="" class="mt-2">Full name</label>
    <input type="text" class="form-control" id="fullname" required placeholder="fullname"/> 
    <input type="submit" id="btn"class="btn btn-success mt-2"/>

    </form>

        </div>
        <div class="col-md-5 ml-5">
        <table class="table table-bordered table-striped table-hover ">
            <thead class="bg-secondary text-light">
                <tr>
                    <th>Sr No.</th>
                    <th>Fullname</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="mydata">

            </tbody>
        </table>
    </div>
    </div>
    </div>
    <div class="modal" id="editmodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h3 class="text-center"> Edit</h3>
            </div>
            <div class="modal-body">
            <label for="">Username</label>
            <input type="text" id="usernameedit" class="form-control"/>
            <label for="">Fullname</label>
            <input type="text" id="fullnameedit"     class="form-control"/>
            <button class="btn btn-success mt-2 ">Edit</button>
            </div>
        </div>
        </div> 
    </div>

    <script>
    $(function(){
        $('#btn').click(function(){
        let user=$('#username').val();
        let pass=$('#password').val();
        let fullname=$('#fullname').val();
            $.ajax({
                url:"2.4.19(ajaxregisterdb).php",
                type:"post",
                data:{username:user,password:pass,fullname:fullname},
                success:function(data){
                    alert(data);
                    getdata(); // 4.4.19(done on next day).
                }
            });
        });
    });
    function getdata(){
        $.ajax({
            url:"2.4.19(ajaxregisterdb).php",
            type:"post",
            data:{getdata:''},
            success:function(data){
                $('#mydata').html(data); //save the data in a id using html function.
            }
        });
    }
    
    function deletedata(id){
        $.ajax({
            url:'2.4.19(ajaxregisterdb).php',
            type:"post",
            data:{deleteid:id},
            success:function(data){
                alert(data);
                getdata(); // it call the fn without refresh and do the action.
            }
        });

    }
    function editdata(id){
        $.ajax({
            url:'2.4.19(ajaxregisterdb).php',
            type:'post',
            data:{singleuserid:id},
            success:function(data){
                $('#usernameedit').val(data.username);
                $('#fullnameedit').val(data.name);
                // alert(JSON.stringify (data));
            }
        });
    }
    getdata(); //called the data
    </script>
</body>
</html>