<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
  <script src="jquery.js"></script> 
  <style>
  .font{
    font-family: 'Iceberg', cursive;
  }
  </style>
</head>
<body>
<div class="container">
  <div class="row ">
  <div class="col-md-12">
              <?php
              if(isset($_POST['sendotp'])){
            require('textlocal.class.php');
            require('textlocalcredention.php');

            $textlocal = new Textlocal(false,false,API_KEY);

            $numbers = array($_POST['mobile']);
            $sender = 'TXTLCL';
            $otp=mt_rand(1000,9999);
            $message=$_POST['message'].$_POST['uname'].$otp;

            try {
                $result = $textlocal->sendSms($numbers, $message, $sender);
                setcookie('otp',$otp);
                echo "OTP successfully send";
            } catch (Exception $e) {
                die('Error: ' . $e->getMessage());
            }
          }
          if(isset($_POST['verify'])){
            $otp=$_POST['otp'];
            if($_COOKIE['otp']== $otp){
              echo 'Congratulation , your mobile is verified';
            }else{
              echo "please enter correct otp";
            }
          }
            ?>
  </div>
    <div class="col-md-6 bg-info mx-auto mt-5">
    
  <h4 style="font-family: 'Dancing Script', cursive; text-align:center; font-weight:bold;">Please Fill all the details and get your OTP</h4>
         <form role="form" enctype="multipart/form-data" action="" method="post">
            <label class="font" for="uname">Name</label>
              <input type="text" class="form-control" id="uname" name="uname" value="" placeholder="Enter your name" required="">
            <label class="font" for="message">Message Field</label>
              <input type="text" class="form-control" id="message" name="message" value="" placeholder="Enter your message here" required="">
            <label class="font" for="mobile">Mobile</label>
              <input type="text" class="form-control" id="mobile" name="mobile" value=""  placeholder="Enter your mobile no" required="">
            <button type="submit" name="sendotp" class="btn btn-lg btn-success btn-block mt-2"> Send OTP</button>
         </form>
  
  <label class="font" for="otp">OTP</label>
  <input type="text" class="form-control" id="otp" name="otp" placeholder="enter your otp"/>
  <button class="btn btn-success btn-block mt-2 mb-3" name="verify" type="submit"> Submit</button>
  </div>
  </div>
</div>

</body>
</html>

