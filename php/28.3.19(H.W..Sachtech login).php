<?php
include_once('connectdb.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Niconne" rel="stylesheet">
    <style>
        body{
            background: rgba(208, 204, 204, 0.415);
        }
    .box{
        width:350px;
        height:auto;
        background: white;
        position: relative;
        display:block;
        margin:auto;
        margin-top:-7%;
        padding:40px;
        box-sizing:border-box;
    }
    .blur{
        color:rgba(128, 128, 128, 0.442);
    }
    .form-control{
        border:2px solid rgba(128, 128, 128, 0.333);
    }
    </style>
</head>
<body>
    <?php
    if(isset($_POST['username'])){
        $username=$_POST['username'];
        $password=$_POST['password'];
        $password=md5($password);
        // echo $password;
        $qry="SELECT * FROM username WHERE username='$username' AND password='$password'";
        $result=$connect->query($qry);
        if($result->num_rows>0){
            echo "success";
            $_SESSION['login']=true;
            header('Location:sachtechdashboard.php');
        }else{
            $_SESSION['login']=false;
        }
       }
    ?>
    <div class="container-fluid" >
        <div class="row" style=" background-attachement:fixed; background-size:cover;">
            <div class="col-md-12 bg-info" style="font-weight:bolder;  color:white; font-family: 'Niconne', cursive; text-align:center;padding:150px;">
                <h1 style="font-size: 50px;">SachTech Login</h1>
                
            </div>
            <div class="col-md-12 bg-secondary " style="font-weight:bolder; color:black; padding:60px;  ">
            <div class="box">
                    <h2 style="text-align:center;" ><i  class="fas fa-user h4"></i>SIGN IN</h2><hr>
                    <form action="" method="post" class="blur">
                        <label for="">USERNAME</label>
                        <input type="text" class="form-control" name="username" required placeholder="username"/>
                        <label for="">PASSWORD</label>
                        <input type="text" class="form-control mb-2" name="password" required placeholder="password"/>
                        <a href="sachtechregister.php" style="font-weight: normal" >Don't Have Account ?</a>
                        <button type="submit" class="form-control bg-success border-white text-white h2 mt-3"><i class="fas fa-sign-in-alt mr-2"></i>Submit</button>
                    </form>
            </div>
            </div>
        </div>
    </div>
    
</body>
</html> 