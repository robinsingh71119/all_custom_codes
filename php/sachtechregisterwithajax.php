
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Niconne" rel="stylesheet">
    
</head>
<body class="bg-info">
<div class="container">
    <div class="row mt-5">
    <div class="col-md-6 mx-auto bg-light">
    <form action="sachtechloginwithajax.php" method="post">
    <h2 style="text-align:center; text-decoration:underline">Register Form</h2>
    <!-- <label for="">Pre title</label>
    <select name="pretitle" id="pretitle" class="form-control" style="width:100px;">
    <option value="1">Mr</option>
    <option value="2">Mrs</option>
    </select> -->
    <label for="">Username</label>
    <input type="text" name="username" id="username" required placeholder="username" class="form-control"/>
    <div class="output"></div>
    <label for="">Fullname</label>
    <input type="text" name="fullname" id="fullname" required placeholder="fullname" class="form-control"/>
    <label for="">Password</label>
    <input type="password" name="password" id="password" required placeholder="password" class="form-control"/>
    <label for="">Father Name</label>
    <input type="text" name="fathername" id="fathername" required placeholder="fathername" class="form-control"/>
    <label for="">Mother Name</label>
    <input type="text" name="mothername" id="mothername" required placeholder="mothername" class="form-control"/>
    <label for="">D.O.B</label>
    <input type="date" name="dob" id="dob" placeholder="dob" required class="form-control"/>
    <p>Address</p>
    <input name="address" required id="address" class="form-control" />
    <input type="submit" class="btn btn-success mx-auto btn-auto mb-3 mt-2" id="registerbtn" />

    </form>
    </div>
</div>
<script>
    $(function(){
    $('#username').keyup(function(){
        let firstname = $('#username').val();
        $.ajax({
            url:"30.3.19(getdata).php",
            type:'post',
            data:{username:firstname},
            success:function(data){
                $('.output').html(data);
            }
        });
    });
});
$(function(){
   
    $('#registerbtn').click(function(){
        let user=$('#username').val();
        let full=$('#fullname').val();
        let pass=$('#password').val();
        let father=$('#fathername').val();
        let mother=$('#mothername').val();
        let dob=$('#dob').val();
        let address=$('#address').val();
        $.ajax({
            url:'sachtechajaxdb.php',
            type:'post',
            data:{username:user,fullname:full,password:pass,fathername:father,mothername:mother,dob:dob,address:address},
            success:function(data){
                alert('fill all details');
                //  window.location='http://localhost/php/sachtechloginwithajax.php';

            }
        });
    });
});


</script>
</body>
</html>