<?php 
    $connect = new mysqli("localhost","root","","firstdatabase");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/> 
</head>
<body>
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <table class="table">
    <thead>
    <tr>
    <th>full name</th>
    <th>Username</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php 
    $qry="SELECT * FROM username";
    $result=$connect->query($qry);
    while($row=$result->fetch_assoc()){
        echo "<tr>";
        echo "<td>".$row['name']."</td>";
        echo "<td>".$row['username']."</td>";
        echo "<td>
            <a href='delete.php?id=".$row['id']."' class='btn btn-danger'>Delete</a>
        
            <a href='edit.php?id=".$row['id']."' class='btn btn-warning'>Edit</a>
        </td>";
        echo "</tr>";
    }
?>
    </tbody>
    </table>

    </div>
  </div>
</div>
</body>
</html>