<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
</head>
<body>
    <button id="showdata">showdata</button>
    <div class="loading">Loading...</div>
    <input type="text" name="name" placeholder="name"/>
    <ul class="mydata"></ul>

    <script>
        $(function(){
            $('.loading').css('display','none');
            $('#showdata').click(function(){
                $('.loading').css('display','block');
                $.ajax({
                    url:'https://restcountries.eu/rest/v2/all',
                    success:function(data){
                        $('.loading').css('display','none');
                        // alert(JSON.stringify(data));
                        let html="";
                        for(m in data){
                            // we can use html=html+""; instead of below thing.
                            html +="<li>"+data[m].name+"</li>";
                        }
                        $('.mydata').html(html);
                    }
                });
            });
        });
        </script>
</body>
</html>