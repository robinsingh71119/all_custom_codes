<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array in Php</title>
    
</head>
<body>
    <?php     
    // Associative
    // Multidimentional
    // Indexed
    $a=['Hello','there','I','am','fine'];  //Index arraya, in this long sttring is there and the value stored in array is shown .
    echo $a['2'] . "<br/>"; 
    $b=['hello',['there']];
    echo $b=[1][0]. "<br/>";
    $c=['name'=>'Robin singh',  //associative array, it has 
        'fname'=>'called it'
        ];
    $d=[
        'name'=>'peter',   //muntidimentional
        'detail'=>[
        'fname'=>'james',
        'mother'=>'donal'
        ]
        ];
           
    echo $c['name']. "<br/>" ;
    echo $d['detail']['fname']. "<br/>";
    echo $d['detail']['mother']. "<br/>";
    $e=[
        'name'=>['firstname'=>'Robin','lastname'=>'Singh'],
        'address'=>'',
        'contact'=>'',
        'email id'=>'sdfnjsd@.',
        'about'=>''
    ];
    echo $e['name']['firstname']. "<br/>";
    echo $e['email id'];
    echo "<pre>";

    print_r($e);
    echo "</pre>";
    
    // if we don't know how the values are stored in the array

    $f=['james',['name'=>'james',['Hello']]];

    echo "<pre>";

    print_r($f);
    echo "</pre>";
    echo $f[1][0][0];


    $p=['myar',['name'=>['james']]];
    echo "<pre>";

    print_r($p);
    echo "</pre>";
    echo $p[1]['name'][0];

    $h=['name'=>'james',
        'fname'=>'Parker',
        'marks'=>['maths'=>'25','science'=>'54','english'=>'30']

        ];
        echo "<pre>";
        print_r($h);
        echo "</pre>";
        echo $h['name']."</br> ";
        echo $h['marks']['maths']."</br>";


        $z=['hello','there','i','am','here']; //index array
        for($i=0; $i<count($z);$i++){ //count is a predefined function used in for loop to run the i till values are stored in the z array.
            echo $z[$i]."</br>";
        };
    ?>
</body>
</html>