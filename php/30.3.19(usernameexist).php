<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

</head>
<body>
<!--/ <form action="" method="post">-->
<input type="text" id="fname" name="firstname" placeholder="firstname"/>
<input type="button" id="btn" value="my Btn" />
<div class="output"></div>
<!--</form>-->
<script>
$(function(){
    $('#fname').keyup(function(){
        let firstname = $('#fname').val();
        $.ajax({
            url:"30.3.19(getdata1).php",
            type:'post',
            data:{fname:firstname},
            success:function(data){
                $('.output').html(data);
            }
        });
    });
});
</script>
</body>
</html>