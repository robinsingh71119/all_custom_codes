<?php
session_start();    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="js/bootstrap.js"></script>
    <script src="jquery.js"></script>
</head>
<body class="bg-light text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-6 bg-dark mt-5 mx-auto">
        <h2 class="text-center mt-3">Login With Ajax</h2>
        <form action="" method="post" id="mylogin">
        <label for="">Username</label>
        <input type="text" id="username" class="form-control" placeholder="username">
        <label for="">Password</label>
        <input type="password" id="password" class="form-control" placeholder="password">
        <input type="submit" id="login" class="btn btn-info mt-3 mb-3">
        <p>Don't have a Account! <a href="http://localhost/php/classajaxcrud.php">Creat Now</a></p>
        </form>
        </div>
      </div>
    </div>
    <script>
    $(function(){
        $('#mylogin').submit(function(event){
            event.preventDefault();
            let user= $('#username').val();
            let pass= $('#password').val();
            $.ajax({
                url:'classajaxdb.php',
                type:'post',
                data:{username:user,password:pass,login:''},
                success:function(data){
                    if(data=='success'){
                        window.location ="http://localhost/php/classajaxdashboard.php";
                    }else{
                        alert(data);
                    }
                }

            });
        });
    });
    </script>
</body>
</html>