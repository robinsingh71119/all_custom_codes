<?php
  // multidim.
$arr=[
    ['name'=>'james1','fname'=>'harry1'],
    ['name'=>'james2','fname'=>'harry2'],
    ['name'=>'james3','fname'=>'harry3'],
    ['name'=>'james4','fname'=>'harry3'],
    ['name'=>'james5','fname'=>'harry4'],

];
   
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>table in php</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
</head>
<body>
    <table class="table table-striped table-hover mx-auto ">
    <thead class="thead">
        <tr>
        <th>S.No </th>
        <th>Name </th>
        <th>Father Name </th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $id=1;
        foreach($arr as $ar){
            echo "<tr>";
            echo "<td>$id</td>";
            echo "<td>".$ar['name']."</td>";
            echo "<td>".$ar['fname']."</td>";
            echo "</tr>";
            $id++;
        }
        ?>
    </tbody>
    </table>
</body>
</html>