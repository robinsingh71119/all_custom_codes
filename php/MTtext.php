<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
</head>
<body class="bg-danger">
<div class="container">
<div class="row">
<div class="col-md-4 mx-auto mt-5 bg-light p-3  ">
<form action="" >
    <h2 class="text-center">Get Country Info</h2><hr>
    <label for="">Enter name</label>
    <input type="text" name="username" class="form-control" placeholder="Enter name"/>
    <input type="submit" class="btn btn-success mt-2 btn-inline"/>
    
    </form>
    <?php
    if(isset($_GET['username'])){
        $username=$_GET['username'];
        $data=file_get_contents('https://restcountries.eu/rest/v2/name/'.$username.'?fullText=true');
      
        $phparr=json_decode($data,true);
        
        foreach($phparr as $d){
            echo "Country: ".$d['name']."<br/>";
            echo "Capital: ".$d['capital']."<br/>";
            }
            $data1=file_get_contents('https://api.openweathermap.org/data/2.5/forecast?q='.$d['capital'].'&mode=&appid=8e7eb1f1951ec6d430a04e15d5011678&units=metric');
            $phparr1=json_decode($data1,true);
            foreach($phparr1['list'] as $e){
                echo "Capital temp: ".$e['main']['temp'];
            }
        
    }
    
    ?>
</div>
</div>
</div>
    
</body>
</html>