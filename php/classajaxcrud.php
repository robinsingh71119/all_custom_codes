<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="js/bootstrap.js"></script>
    <script src="jquery.js"></script>
</head>
<body class="bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-6 mt-5 mx-auto bg-light">
          <h2 class="text-center">Register With Ajax</h2>
            <form action="" method="post" id="myform">
              <label for="">Username</label>
              <input type="text" name="username" required id="username" class="form-control" placeholder="username"/>          
              <label for="">Password</label>
              <input type="password" name="password" required id="password" class="form-control" placeholder="password"/>
              <label for="">Fullname</label>
              <input type="text" name="fullname" required id="fullname" class="form-control" placeholder="fullname"/>
              <input type="submit" id="register" class="btn btn-success mt-3 mb-3">          
            </form>  
        </div>
      </div>
    </div>
    <script>
    $(function(){
        $('#myform').submit(function(event){
            event.preventDefault();
            let user=$('#username').val();
            let pass=$('#password').val();
            let fullname=$('#fullname').val();
            $.ajax({
                url:'classajaxdb.php',
                type:'post',
                data:{username:user,password:pass,fullname:fullname,register:''},
                success:function(data){
                    if(data=='success'){
                      alert(data);
                     window.location ="http://localhost/php/classajaxlogin.php";
                    }else{
                      alert('Error');
                    }
                    
                }
            });
        });
    });
    </script>
</body>
</html>