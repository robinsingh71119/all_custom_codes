<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
</head>
<body class="bg-danger">
<div class="container">
<div class="row">
<div class="col-md-4 mx-auto mt-5 bg-light p-3  ">
<form action="" >
    <h2 class="text-center">Get Country Info</h2><hr>
    <label for="">Enter name</label>
    <input type="text" name="username" class="form-control" placeholder="Enter name"/>
    <input type="submit" class="btn btn-success mt-2 btn-inline"/>
    
    </form>
    
    <script>
    $(function(){
        $('.submit').click(function(){
            $.ajax({
                url:'https://restcountries.eu/rest/v2/name/'.$username.'?fullText=true',
                type:"post",
                success:function(data){
                    echo "Country: ".$data['name']."<br/>";
                    echo "Capital: ".$data['capital']."<br/>"; 
                }
            });
        });
    });
    </script>
</div>
</div>
</div>
    
</body>
</html>