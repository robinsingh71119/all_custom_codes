<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Niconne" rel="stylesheet">
    <style>
    body{
        background: rgb(201, 197, 197);
    }
    .user{
        width:50px;
        height:50px;
        border-radius: 50px;
        text-align: center;
        margin-top:20px;
        background: black;
        color:white;
        line-height: 50px;
    }
    .nav-link{
        color:white;
        font-size: 16px;
        padding: 10px;
    }
    .nav-link:hover{
        background: black;
        border-left: 2px solid #01695e;
        color:white;
        
    }
    </style>
</head>
<body>
   <div class="container-fluid">
       <div class="row " style="background: #009688">
           <div class="col-md-2 " style="background:#01695e">
               <h2 style="text-align:center; display:inline; color:white;font-family: 'Niconne', cursive;">Sach tech</h2>
           </div>
       </div>
       <div class="row ">
           <div class="col-md-2  pt-2" style="background: #222d32; height:752px;"> 
            <div class="user" >R</div>
            <span class=" h2 text-light" >ROBINSINGH</span><br/>
            <small class="h6 text-light">intern</small>
            <ul class="navbar-nav "  >
                <li class="nav-item"><a class="nav-link" href=""><i class="fas fa-tachometer-alt"></i>Dashboard</a></li>
                <li class="nav-item"><a class="nav-link" href=""><i class="fas fa-graduation-cap"></i>Attendance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;></a></li>
                <li class="nav-item"><a class="nav-link" href="28.3.19(H.W..Sachtech login).php"><i class="fas fa-sign-in-alt mr-2"></i>Log Out</a></li>
                
            </ul>
           </div>
           <div class="col-md-8 bg-white p-3" style="height:100px;">
                <h4><i class="fas fa-tachometer-alt"></i>Dashboard</h4>
                <h6 style="font-size: 12px;">A first page after login.</h6>
            </div>
            <div class="col-md-2 bg-white p-5" style="height: 100px;">
                    <a href="" ><i style="color:black" class="fas fa-home"></i>/Dashboard</a>
            </div>
            <div class="row">
                <div class="col-md-12 ">

                </div>
            </div>
       </div>
       
   </div>
</body>
</html>