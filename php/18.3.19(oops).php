<?php
class first{
    public $a = 10;  //member
    protected $s=20;
    public $b=20; //member
    public function hello(){   // buy default the variables are  public. //member
        return"this is my function"."<br/>";
    }
    public function sum(){  //member
        return $this->a+$this->b;  //here this function is used to make the sum of variables into the class
    }
}
$obj = new first();
echo $obj->a."<br/>";
echo $obj->hello();
echo $obj->sum()."<br/>";

class second extends first{ //inheritence is done here, where extends class is used to inheritence the properties of the above class.
    public $c=30; //single inheritence
    function add(){

        return $this->a+$this->b+$this->c;
        

    } 
}

$ob=new second();
echo $ob->add()."<br/>";



class third extends second{ //multiple inheritence
    public $d=40;
    function add1(){
        return $this->a+$this->b+$this->c+$this->d;
    }
}
$obbj=new third();
echo $obbj->add1()."<br/>";


class tryprotected extends first{ //in this protected variable is called .
    public $e=30; 
    function add2(){
        return $this->s;
        
        

    }
}

$ob1=new tryprotected();
echo $ob1->add2()."<br/>";



?>