<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Dokdo" rel="stylesheet">
    <style>
    .box input{
        width:100%;
        box-sizing:border-box;
        box-shadow:none;
        border:none;
        border-bottom:3px solid grey;
     }
    /*.box form div{
        position:relative;
    }
    .box form div label{
        position:absolute;
        top:10px;
        left:0;
        color:#999;
        transition:.5s;
        pointer-events:none;
    }
    .box input:focus ~ label,
    .box input:valid ~ label{
        top:-12px;
        left:0;
        color:red;

    }
    .box input:focus,
    .box input:valid{
        border-bottom:2px solid blue;
    } */



    </style>

</head>
<body>
    <div class="container-fluid">
        <div class="row " >
            <div class="col-md-4 box" >
            <form action="" method="post">
                <h2 class="text-center" style="font-family: 'Indie Flower', cursive; text-decoration:underline;">Register</h2>
                <div >
                <label for="username">Username</label>
                <input type="text" id="username" placeholder="username" class="form-control"/></div>
                <div>
                <label for="password">Password</label>
                <input type="text" id="password" placeholder="password" class="form-control"/></div>
                <div>
                <label for="fullname">Fullname</label>
                <input type="text" id="fullname" placeholder="fullname" class="form-control"/></div>
                <div>
                <label for="">Father Name</label>
                <input type="text" id="fathername" placeholder="fathername" class="form-control"/></div>
                <div>
                <!-- <input type="date"> -->
                <label for="">Mother Name</label>
                <input type="text" id="mothername" placeholder="mothername" class="form-control"/></div>
                <input type="submit" id="registerbtn" class="btn btn-success mt-2"/>
            </form>
            </div>
            <div class="col-md-8">
              <table class="table table-bordered table-striped table-hover ">
              <h2 class="text-center">Registered Data</h2>
                  <thead class="h6">
                      <tr>
                          <th>S.no</th>
                          <th>Username</th>
                          <th>Password</th>
                          <th>Fullname</th>
                          <th>Father Name</th>
                          <th>Mother Name</th>
                          <th colspan="3" class="text-center">Action</th>
                          
                      </tr>
                  </thead>
                  <tbody id="mydata">

                  </tbody>
              </table>
            </div>
        </div>
</div>
<div class="modal" id="editmodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Edit</h3>

            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <label for="">Username</label>
                <input type="text" id="usernameedit" placeholder="username" class="form-control"/>
                <label for="">Full Name</label>
                <input type="text" id="fullnameedit" placeholder="username" class="form-control"/>
                <label for="">Father Name</label>
                <input type="text" id="fathernameedit" placeholder="username" class="form-control"/>
                <label for="">Mother Name</label>
                <input type="text" id="mothernameedit" placeholder="username" class="form-control"/>
                <input type="hidden" id="editid"/>
                <button class="btn btn-info mt-2" onclick="updatedata()">Edit</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="changemodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Change Password</h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <label for="">New Password</label>
                <input type="text" id="editpassword" placeholder="New Password" class="form-control"/>
                <input type="hidden" id="passwordid"/>
                <button class="btn btn-info mt-2" onclick="editpass()">Change</button>
            </div>
        </div>
    </div>
</div>
<script>
    
$(function(){
    $('#registerbtn').click(function(){
    let user=$('#username').val();
    let pass=$('#password').val();
    let full=$('#fullname').val();
    let father=$('#fathername').val();
    let mother=$('#mothername').val();
    $.ajax({
        url:'4.4.19(newtableconnect).php',
        type:'post',
        data:{username:user,password:pass,fullname:full,fathername:father,mothername:mother},
        success:function(data){
            alert(data);
              window.location='http://localhost/php/sachtechloginwithajax.php';
        }
    });
    });

});
function getdata(){
    $.ajax({
        url:'4.4.19(newtableconnect).php',
        type:'post',
        data:{getdata:''},
        success:function(data){
            $('#mydata').html(data);
        }
    });
}
function deletedata(id){
$.ajax({
    url:'4.4.19(newtableconnect).php',
    type:'post',
    data:{deleteid:id},
    success:function(data){
        alert(data);
        getdata();
    }
});
}
function editdata(id){
    $.ajax({
        url:'4.4.19(newtableconnect).php',
        type:'post',
        data:{singleuserid:id},
        success:function(data){
            $('#editid').val(id); // 5.4.19(new work )to declare id for edit data.
            $('#usernameedit').val(data.username);
            $('#fullnameedit').val(data.fullname);
            $('#fathernameedit').val(data.fathername);
            $('#mothernameedit').val(data.mothername);
            // alert(JSON.stringify(data));
        }
    });
}
function updatedata(){
    let username =$('#usernameedit').val();
    let fullname =$('#fullnameedit').val();
    let fathername =$('#fathernameedit').val();
    let mothername =$('#mothernameedit').val();
    let id=$('#editid').val();
    $.ajax({
        url:'4.4.19(newtableconnect).php',
        type:'post',
        data:{usernameedit:username,fullnameedit:fullname,fathernameedit:fathername,mothernameedit:mothername,editid:id},
        success:function(data){
            $('#editmodal').modal('toggle');
            getdata();

        }
    });
}
function changedata(id){
    $('#passwordid').val(id);
}
 function editpass(){
    let newpass=$('#editpassword').val();
    let id=$('#passwordid').val();
    $.ajax({
        url:'4.4.19(newtableconnect).php',
        type:'post',
        data:{passwordedit:newpass,passid:id},
        success:function(data){
            alert(data);
            $('#changemodal').modal('toggle');
            getdata();
        }
    });

}




getdata();
</script>
</body>
</html>