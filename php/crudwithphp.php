<?php
$connect=new mysqli('localhost','root','','classdb');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <style>
    th,td{
        
        padding:10px;
    }
    .btn{
        width:80px;
    }
    </style>
</head>
<body class="bg-dark">
<?php
if(isset($_POST['username'])){
     $username = $_POST['username'];
     $fullname = $_POST['fullname'];
     $password = $_POST['password'];
     
    $qry="INSERT INTO trytable(username,password,fullname)VALUES('$username','$password','$fullname')";
    if($connect->query($qry)){
        echo "<h2>Success</h2>";
    }else{
        echo "error".$connect->error;
    }
}
?>


    <div class="container-fluid">
      <div class="row pl-5">
        <div class="col-md-5 bg-light mt-5 ml-5 ">
        <h2 class="text-center">Register Page</h2>
        <form action="" method="post">
        <label for="">Username</label>
        <input type="text" class="form-control" placeholder="username" name="username">
        <label for="">Fullname</label>
        <input type="text" class="form-control" placeholder="fullname" name="fullname">
        <label for="">Password</label>
        <input type="password" class="form-control" placeholder="password" name="password">
        <div class="text-center">
        <input type="submit" class="btn btn-success mt-3 mb-3">
        </div>
        </form>
        </div>
        <div class="col-md-5 mt-5 bg-info ml-5">
        
        <caption>Register Data</caption>
        <table class=" mt-3">
        <thead>
        <tr>
            <th>Username</th>
            <th>Fullname</th>
            <th>password</th>
        </tr>
        </thead>
        <tbody>
        <?php 
    $qry1 = "SELECT * FROM trytable";
    $result=$connect->query($qry1);
    while($row=$result->fetch_assoc()){
        echo "<tr>";
        echo "<td>".$row['username']."</td>";
        echo "<td>".$row['fullname']."</td>";
        echo "<td>".$row['password']."</td>";
        echo "<td>
            <a href='phpcruddelete.php?id=".$row['id']."' class='btn btn-danger'>Delete</a>
        
            <a href='phpcrudedit.php?id=".$row['id']."' class='btn btn-warning'>Edit</a>
        </td>";
        echo "</tr>";
    }
?>
        </tbody>
        </table>
        </div>
      </div>
    </div>
</body>
</html>